# Cachable Database Settings with Laravel

Do you want to have more dynamic configuration system than files based provided with Laravel? 
This is good reason to use this package. You can edit/create and retrive your settings from database using simple
facade. Additionaly it supports json decoding and encoding, so you can store also jsonable values.
## Content
1. [Requirements](#requirements)
2. [Setup](#setup)
3. [Configuration](#configuration)
4. [Usage](#usage)
    * [Database](#database)
    * [Cache](#cache)
    * [Examples](#examples)


## Requirements
This package has been written with PHP 7.2 for Laravel 5.7 and Lumen 5.7, so I don't guarantee that on olders versions 
this will work stable. 
	
## Setup
To download this package just write in your project terminal:
```
$ composer require kromacie/l5settings
```

When the package will be downloaded, register the service provider.

For Lumen:
```php
$app->register(\Kromacie\L5Settings\SettingsServiceProvider::class);
```

For Laravel this should be discovered automatically but if not, just register this in config/app.php
using the same namespace as above.

Next, copy or provide configuration file.
You can find this in "vendor/kromacie/l5settings/config" directory.

Now, you have to provide migration file and run that migration. Otherwise registering settings will throw SettingsNotLoaded exception.

The last thing that you should do, is set enable to true in config file.   

## Configuration

Configuration file contains many parameters which can help you to adapt how your 
settings should be stored, retrived and which parameters it should overwrite.

```php
<?php

return [

    /*
     * Enable or disable this package.
     */
    'enabled' => false,

    /*
     * Here you can set which Laravel configuration values should be overwritten by settings
     * from database.
     */
    'overwrite' => [

    ],

    'database' => [

        /**
         * Set the connection of using database.
         */
        'connection' => 'mysql',

        /*
         * Just set how your table for storing settings should be named.
         */
        'table' => 'settings',

    ],

    'cache' => [

        /*
         * Set which cache store should be used to store your settings.
         */
        'store' => 'redis',

        /*
         * All of the settings retrived from database will be stored under this cache key.
         * You can change this as you wish.
         */
        'key' => 'settings',

        /*
         * Time in minutes of - "How long values should be cached?"
         */
        'expiration_time' => 60,

    ],

    /*
     * Set the prefix under which you can find your settings using config function.
     */
    'prefix' => 'global',

    /*
     * You can set custom SettingManager handler to resolve how your settings should be
     * stored or retrived.
     */
    'manager' => \Kromacie\L5Settings\SettingsManager::class,

    /*
     * You can set your custom model if u want to change some logic inside
     */
    'model' => \Kromacie\L5Settings\Models\Setting::class

];

```

## Usage

### Database
Usage of this package is pretty simple. After configuration you should use Settings facade if you want to interfere with your database. It contains some important methods like:

 * @method static mixed set($key, $value);
 * @method static mixed get($key);
 * @method static boolean has($key);
 * @method static array all();
 * @method static void remove($key);
 * @method static void removeAll();
 
 Method set is responsible for creating and updating fields in database.
 Methods get, has, all are doing exactly this as they are called. Also, using set, remove and removeAll will flush
 cache of your settings and register them again.
 
 I don't recommend using remove and removeAll expectially in production, becouse 
 settings shouldn't be removed.
 
 ### Cache
 If u want to not use your database connection, you should use configuration from
 Config/Repository to retrive your configuration.
 
 For example, if you have prefix named "global", all retrived settings from 
 database will be under this prefix. And if you want to get one of retrived setting, 
 you should use this like:
 
 ```php
 $foo = config('global.foo');
 ```
 Or using Facade:
 ```php
 $foo = Config::get('global.foo');
 ```
 Also after injection of Config/Repository.
 ```php
 public function controllerEndpoint(Repository $config)
 {
    $foo = $config->get('global.foo');
 }
 ```
 
 ### Overwriting
 
 For some cases usefull can be overwriting other config by your settings. Assuming that you want
 to have possibility to change default language of your app. You can store it under 'fallback_locale' in your database, 
 and add this parameter to config file.
 
 ```php
 /**
  * config/l5settings.php
  */
  
 return [
    'overwrite' => [
        'fallback_locale' => 'app.fallback_locale'
    ]
 ]
 ```
 
 And now, after service providers boot, app.fallback_locale will contains the same
 value as fallback_locale in the database.
 
 I hope that you will enjoy this package. If you find some issues, please report them for me.
 Thanks.