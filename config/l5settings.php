<?php

return [

    /*
     * Enable or disable this package.
     */
    'enabled' => env("SETTINGS_ENABLED", false),

    /*
     * Here you can set which Laravel configuration values should be overrided by settings
     * from database.
     */
    'overwrite' => [

    ],

    'database' => [

        /**
         * Set the connection of using database.
         */
        'connection' => 'mysql',

        /*
         * Just set how your table for storing settings should be named.
         */
        'table' => 'settings',

    ],

    'cache' => [

        /*
         * Set which cache store should be used to store your settings.
         */
        'store' => 'redis',

        /*
         * All of the settings retrived from database will be stored under this cache key.
         * You can change this as you wish.
         */
        'key' => 'settings',

        /*
         * Time in minutes of - "How long values should be cached?"
         */
        'expiration_time' => 60,

    ],

    /*
     * Set the prefix under which you can find your settings using config function.
     */
    'prefix' => 'global',

    /*
     * You can set custom SettingManager handler to resolve how your settings should be
     * stored or retrived.
     */
    'manager' => \Kromacie\L5Settings\SettingsManager::class,

    /*
     * You can set your custom model if u want to change some logic inside
     */
    'model' => \Kromacie\L5Settings\Models\Setting::class

];
