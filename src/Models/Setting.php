<?php namespace Kromacie\L5Settings\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $primaryKey = false;
    public $timestamps = false;

    protected $fillable = [
        'name', 'value'
    ];
}