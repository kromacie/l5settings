<?php namespace Kromacie\L5Settings;

use Exception;
use Illuminate\Cache\CacheManager as Cache;
use Illuminate\Events\Dispatcher;
use Illuminate\Support\Carbon;
use Illuminate\Support\ServiceProvider;
use Kromacie\L5Settings\Contracts\SettingInterface;
use Kromacie\L5Settings\Events\SettingsModified;
use Kromacie\L5Settings\Exceptions\SettingsNotLoaded;
use Kromacie\L5Settings\Listeners\SettingsModifiedListener;

class SettingsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     * @throws SettingsNotLoaded
     */

    public function boot()
    {

        $date = Carbon::now();

        if(!$this->isLumen()) {
            $this->publishes([
                __DIR__ . '/../config/l5settings.php' => config_path('settings.php')
            ], 'config');

            $this->publishes([
                __DIR__.'/../database/migrations/migration.php.stub' => database_path("migrations/{$date->format('Y_m_d_His')}_create_settings_table.php")
            ], 'migrations');

        }

        $this->registerEvents();
        $this->registerBindings();
        try {
            $this->registerSettings();
        } catch (Exception $exception) {
            throw new SettingsNotLoaded($exception->getMessage());
        }

    }

    public function getConfig()
    {
        return $this->app->get('config')
            ->get('l5settings');
    }


    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__ . '/../config/l5settings.php',
            'l5settings'
        );
    }

    public function registerEvents()
    {
        /** @var Dispatcher $events */
        $events = $this->app['events'];

        $events->listen(SettingsModified::class, SettingsModifiedListener::class);

    }

    public function registerBindings()
    {
        /** @var Dispatcher $events */
        $events = $this->app['events'];

        /** @var Cache $cache */
        $cache = $this->app['cache'];

        $config = $this->getConfig();

        $this->app->singleton(SettingInterface::class, function () use ($config, $events) {
            $class = $config['manager'];
            /** @var SettingInterface $manager */
            $manager = new $class();

            $model = $config['model'];

            $manager->setModel(new $model());
            $manager->setTable($config['database']['table']);
            $manager->setConnection($config['database']['connection']);
            $manager->setDispatcher($events);

            return $manager;
        });

        $this->app->singleton(CacheManager::class, function () use ($config, $cache) {
            $manager = new CacheManager();
            $settings = $this->app->get(SettingInterface::class);

            $manager->setCache(
                $cache->driver($config['cache']['store'])
            );
            $manager->setTime($config['cache']['expiration_time']);
            $manager->setKey($config['cache']['key']);
            $manager->setManager($settings);

            return $manager;
        });

        $this->app->singleton(SettingsRegistrar::class, function () {
            $registrar = new SettingsRegistrar();
            $registrar->setConfig($this->getConfig());
            $registrar->setRepository($this->app->get('config'));
            $registrar->setCacheManager($this->app->get(CacheManager::class));

            return $registrar;
        });
    }

    public function registerSettings()
    {
        $this->app->get(SettingsRegistrar::class)
            ->register();
    }

    public function isLumen()
    {
        if (app() instanceof Application) {
            return false;
        } else {
            return true;
        }
    }


}