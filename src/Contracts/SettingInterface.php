<?php namespace Kromacie\L5Settings\Contracts;

use Illuminate\Database\ConnectionInterface;
use Illuminate\Events\Dispatcher;

interface SettingInterface
{
    public function setTable($table);
    public function setModel($model);
    public function setConnection($connection);
    public function setDatabase(ConnectionInterface $connection);
    public function setDispatcher(Dispatcher $dispatcher);

    public function set($key, $value);
    public function get($key);
    public function has($key);
    public function all();
    public function remove($key);
    public function removeAll();
}