<?php namespace Kromacie\L5Settings\Facades;

use Illuminate\Support\Facades\Facade;
use Kromacie\L5Settings\Contracts\SettingInterface;


/**
 * @method static mixed set($key, $value);
 * @method static mixed get($key);
 * @method static boolean has($key);
 * @method static array all();
 * @method static void remove($key);
 * @method static void removeAll();
 *
 * @see SettingInterface
 */
class Settings extends Facade
{
    public static function getFacadeAccessor()
    {
        return SettingInterface::class;
    }
}