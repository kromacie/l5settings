<?php namespace Kromacie\L5Settings\Listeners;

use Kromacie\L5Settings\Events\SettingsModified;
use Kromacie\L5Settings\SettingsRegistrar;

class SettingsModifiedListener
{

    private $registrar;

    /**
     * Create the event listener.
     *
     * @param SettingsRegistrar $registrar
     */
    public function __construct(SettingsRegistrar $registrar)
    {
        $this->registrar = $registrar;
    }

    /**
     * Handle the event.
     *
     * @param  SettingsModified
     * @return void
     */
    public function handle()
    {
        $this->registrar->register(true);
    }

}
