<?php namespace Kromacie\L5Settings;

use Illuminate\Cache\Repository;


/**
 * Class CacheManager
 * @package Kromacie\L5Settings
 *
 * @property Repository $cache
 * @property SettingsManager $manager
 */
class CacheManager
{

    protected $cache;

    protected $key;

    protected $time;

    protected $manager;

    /**
     * @param mixed $cache
     */
    public function setCache($cache): void
    {
        $this->cache = $cache;
    }

    /**
     * @param mixed $key
     */
    public function setKey($key): void
    {
        $this->key = $key;
    }

    /**
     * @param mixed $time
     */
    public function setTime($time): void
    {
        $this->time = $time;
    }

    /**
     * @param mixed $manager
     */
    public function setManager($manager): void
    {
        $this->manager = $manager;
    }

    public function cache()
    {
        return $this->cache->remember($this->key, $this->time, function (){
            return $this->manager->all();
        });
    }

    public function has()
    {
        return $this->cache->has($this->key);
    }

    public function flush()
    {
        $this->cache->forget($this->key);
    }



}