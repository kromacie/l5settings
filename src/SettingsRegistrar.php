<?php namespace Kromacie\L5Settings;

use Illuminate\Config\Repository;

class SettingsRegistrar
{

    private $config;

    private $manager;

    private $repository;

    public function setConfig($config)
    {
        $this->config = $config;
    }

    public function setCacheManager($manager)
    {
        $this->manager = $manager;
    }

    public function setRepository($repository)
    {
        $this->repository = $repository;
    }

    public function register($fresh = false)
    {
        $enabled = $this->config['enabled'];

        if(!$enabled) {
            return;
        }

        /** @var CacheManager $manager */
        $manager = $this->manager;

        /** @var Repository $config */
        $config = $this->repository;

        if($fresh) {
            $manager->flush();
            $config->set($this->config['prefix'], null);
        }

        $settings = $manager->cache();

        $prefix = $this->config['prefix'];

        $overwrite = $this->config['overwrite'];

        foreach ($settings as $key => $value) {
            $config->set("$prefix.$key", $value);

            if(key_exists($key, $overwrite)) {
                $toOverwrite = $overwrite[$key];
                $toOverwrite = is_array($toOverwrite) ? $toOverwrite : [$toOverwrite];
                foreach ($toOverwrite as $record) {
                    $config->set($record, $value);
                }
            }
        }

    }
}