<?php namespace Kromacie\L5Settings;

use Illuminate\Contracts\Cache\Repository;
use Illuminate\Database\ConnectionInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Events\Dispatcher;
use Kromacie\L5Settings\Contracts\SettingInterface;
use Kromacie\L5Settings\Events\SettingsModified;

/**
 * @property ConnectionInterface $database
 * @property Repository $cache
 * @property Dispatcher $events
 */
class SettingsManager implements SettingInterface
{

    private $table;

    private $connection;

    private $events;

    private $model;

    public function setDatabase(ConnectionInterface $connection)
    {
        $this->database = $connection;
    }

    public function setModel($model)
    {
        $this->model = $model;
    }

    public function setTable($table)
    {
        $this->table = $table;
    }

    public function setConnection($connection)
    {
        $this->connection = $connection;
    }

    /**
     * @param mixed $dispatcher
     */
    public function setDispatcher(Dispatcher $dispatcher): void
    {
        $this->events = $dispatcher;
    }

    public function set($key, $value)
    {
        if (!$this->has($key)) {
            $result = $this->create($key, json_encode($value));
        } else {
            $result = !is_null($this->update($key, json_encode($value)));
        }

        $this->events->fire(new SettingsModified());

        return $result;
    }

    public function has($key)
    {
        return !is_null($this->getQueryForKey($key)->first());
    }

    public function get($key)
    {
        return json_decode($this->getQueryForKey($key)
            ->value('value'));
    }

    public function all()
    {
        $result = $this->getQuery()->get(['name', 'value']);

        $array = [];

        foreach ($result as $item) {
            $array[$item->name] = json_decode($item->value);
        }

        return $array;
    }

    public function remove($key)
    {
        $result = $this->getQueryForKey($key)
            ->delete();

        $this->events->fire(new SettingsModified());

        return $result;
    }

    public function removeAll()
    {
        $result = $this->getQuery()
            ->delete();

        $this->events->fire(new SettingsModified());

        return $result;
    }

    private function getQuery()
    {
        /** @var Model $model */
        $model = $this->model;
        $model->setConnection($this->connection);
        $model->setTable($this->table);
        return $model::query();
    }

    private function getQueryForKey($key)
    {
        return $this->getQuery()->where('name', '=', $key);
    }

    private function create($key, $value)
    {
        return $this->getQuery()->insert([
            'name' => $key,
            'value' => $value
        ]);
    }

    private function update($key, $value)
    {
        return $this->getQueryForKey($key)->update([
            'value' => $value
        ]);
    }

}