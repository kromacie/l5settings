<?php

use Illuminate\Config\Repository;
use Kromacie\L5Settings\Facades\Settings;
use Laravel\Lumen\Application;

class SettingsTest extends \Laravel\Lumen\Testing\TestCase
{

    public function testOfBooting()
    {
        $app = $this->createApplication();
        $this->assertIsObject($app);
    }

    public function testOfCache()
    {
        $app = $this->createApplication();

        $cache = $app->get(\Kromacie\L5Settings\CacheManager::class);

        $this->assertTrue($cache->has());

        $cache->flush();

        $this->assertFalse($cache->has());

    }

    public function testOfFacade()
    {
        $all = Settings::all();

        $this->assertIsArray($all);
    }

    public function testOfCRUD()
    {
        $app = $this->createApplication();

        $cache = $app->get(\Kromacie\L5Settings\CacheManager::class);

        /** @var \Illuminate\Config\Repository $config */
        $config = $app->get('config');

        Settings::set('hehe', ["pah"]);

        $this->assertEquals(true, $config->has('global.hehe'));

        Settings::set('hehe', ["pff"]);

        $this->assertEquals(["pff"], $config->get('global.hehe'));

        Settings::set('hehe', ["pah"]);

        $this->assertEquals(true, $config->has('global.hehe'));

        Settings::set('hehe', ["pff"]);

        $this->assertEquals(["pff"], $config->get('global.hehe'));

        $value = Settings::get('hehe');

        $this->assertEquals(["pff"], $value);

        Settings::remove('hehe');

        $value = Settings::get('hehe');

        $this->assertEquals(null, $value);
        $this->assertEquals(false, $config->has('global.hehe'));
    }

    public function testOfCacheHas()
    {
        $app = $this->createApplication();

        /** @var Illuminate\Config\Repository $config */
        $config = $app->get('config');

        $result = $config->has('global');

        $config = $config->get('global.app.fallback_locale');

        $this->assertEquals("de", $config);
        $this->assertEquals(true, $result);
    }

    public function testOfOverwriting()
    {
        $app = $this->createApplication();

        /** @var Illuminate\Config\Repository $config */
        $config = $app->get('config');

        $config = $config->get('database.migrations');

        $this->assertEquals("de", $config);
    }

    /**
     * Creates the application.
     *
     * Needs to be implemented by subclasses.
     *
     * @return Application
     */
    public function createApplication($boot = true)
    {
        $app =  require __DIR__.'/../../../../bootstrap/app.php';
        if($boot)
            $app->boot();
        return $app;
    }
}